const { Router } = require('express');
const FightService = require('../services/fightService');
const { createFightValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

router.get('', (req, res, next) => {
    try {
        const fights = FightService.getFights();
        res.data = fights;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);


router.post('', createFightValid, (req, res, next) => {
    try {
        const fightData = req.body;
        const fight = FightService.createFight(fightData);
        res.data = fight;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }

}, responseMiddleware)


module.exports = router;