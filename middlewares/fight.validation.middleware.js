const {validateRequest} = require('../validators/request.validator');
const Joi = require('joi');

const createFightValid = (req, res, next) => {
    const schema = Joi.object({
        fighter1: Joi.string().required(),
        fighter2: Joi.string().required(),
        log: Joi.array().required()
    });

    const isValid = validateRequest(req, res, schema);

    if(isValid) {
        next();
    }
}

exports.createFightValid = createFightValid;