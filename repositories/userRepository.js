const { BaseRepository } = require('./baseRepository');

class UserRepository extends BaseRepository {
    constructor() {
        super('users');
    }

    getUserById(id) {
        return this.dbContext.find({ id }).value();
    }

    getUserByEmail(email) {
        return this.dbContext.find({ email }).value();
    }

    getUserByPhoneNumber(phoneNumber) {
        return this.dbContext.find({ phoneNumber }).value();
    }
}

exports.UserRepository = new UserRepository();