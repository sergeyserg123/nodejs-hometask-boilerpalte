const {validateRequest} = require('../validators/request.validator');
const Joi = require('joi');

const createUserValid = (req, res, next) => {
    const schema = Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        email: Joi.string().regex(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@gmail.com$/).required(),
        phoneNumber: Joi.string().regex(/^(?:\+380)?(?:[0-9]{9})$/).required(),
        password: Joi.string().min(3).required()
    });

    const isValid = validateRequest(req, res, schema);

    if(isValid) {
        next();
    }
}

const updateUserValid = (req, res, next) => {
    const schema = Joi.object({
        firstName: Joi.string().required().optional(),
        lastName: Joi.string().required().optional(),
        email: Joi.string().regex(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@gmail.com$/).required().optional(),
        phoneNumber: Joi.string().regex(/^(?:\+380)?(?:[0-9]{9})$/).required().optional(),
        password: Joi.string().min(3).required().optional(),
        createdAt: Joi.optional(),
        updatedAt: Joi.optional()
    });

    const isValid = validateRequest(req, res, schema);

    if(isValid) {
        next();
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;