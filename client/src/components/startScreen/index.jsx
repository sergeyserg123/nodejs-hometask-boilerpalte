import * as React from 'react';
import SignInUpPage from '../signInUpPage';
import { isSignedIn } from '../../services/authService';
import Fight from '../fight';
import SignOut from '../signOut';
import Arena from '../arena';
import ShowWinner from '../showWinner';
import FightsStory from '../fightsStory';


class StartScreen extends React.Component {
    state = {
        isSignedIn: false,
        isFightMode: false,
        isShowFightsStory: false,
        firstFighter: null,
        secondFighter: null,
        winner: null,
        fight: null
    };

    componentDidMount() {
        this.setIsLoggedIn(isSignedIn());
    }

    setIsLoggedIn = (isSignedIn) => {
        this.setState({ isSignedIn });
    }

    setFightMode = (fighter1, fighter2) => {
        this.setState({isFightMode: true, firstFighter: fighter1, secondFighter: fighter2});
    }

    setWinnerPage = (fighter, log) => {
        const fight = this.buildFight(log);
        this.setState({isFightMode: false, winner: fighter, firstFighter: null, secondFighter: null, fight: fight})
    }

    setFightsStoryPage = () => {
        this.setState({isShowFightsStory: true, winner: null, fight: null })
    }

    setNewFight = () => {
        this.setState({
            isFightMode: false,
            isShowFightsStory: false,
            firstFighter: null,
            secondFighter: null,
            winner: null,
            fight: null
        })
    }

    buildFight = (log) => {
        const {firstFighter, secondFighter} = this.state;

        const fight = {
            fighter1: firstFighter.id,
            fighter2: secondFighter.id,
            log: [...log]
        }

        return fight;
    }

    render() {
        const { isSignedIn, isFightMode, winner, fight, isShowFightsStory }  = this.state;

        if (isShowFightsStory) {
            return <FightsStory
                onNewFight={this.setNewFight} />
        }

        if (winner !== null && fight !== null) {
            return <ShowWinner 
                onShowFightsStory={this.setFightsStoryPage}
                winner={winner} 
                fight={fight} />
        }

        if (isFightMode) {
            const {firstFighter, secondFighter} = this.state;
            return <Arena onShowWinner={this.setWinnerPage} firstFighter={firstFighter} secondFighter={secondFighter} />
        }

        if (!isSignedIn) {
            return <SignInUpPage setIsLoggedIn={this.setIsLoggedIn} />
        }
        
        return (
            <>
                <Fight onStartFight={this.setFightMode} />
                <SignOut isSignedIn={isSignedIn} onSignOut={() => this.setIsLoggedIn(false)} />
            </>
        );
    }
}

export default StartScreen;