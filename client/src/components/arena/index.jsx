import * as React from 'react';
import { controls } from '../../constants/controls';
import FighterIndicator from '../fighterIndicator/index';
import './arena.css';

class Arena extends React.Component {
    
    constructor() {
        super();
        this.state = {
            firstFighterState : {
                isProtected: false,
                criticalHitChance: 0,
                dodgeChance: 0,
                fullHealthVal: 0,
                CriticalHitCombinationAvailable: true,
                fighterInfo: {},
                healthBarWidth: 100
            },
              
              secondFighterState : {
                isProtected: false,
                criticalHitChance: 0,
                dodgeChance: 0,
                fullHealthVal: 0,
                CriticalHitCombinationAvailable: true,
                fighterInfo: {},
                healthBarWidth: 100
            }
        }

        this.log = []
    }

    componentDidMount() {
        const {firstFighter, secondFighter} = this.props;
        const {firstFighterState, secondFighterState} = this.state;

        const firstState = {...firstFighterState, fighterInfo: firstFighter, fullHealthVal: firstFighter.health};
        const secondState = {...secondFighterState, fighterInfo: secondFighter, fullHealthVal: secondFighter.health};
        this.setState({firstFighterState: firstState, secondFighterState: secondState}, () => { 
            this.fight().then(fighter => {this.props.onShowWinner(fighter, this.log)})
        })
    }

    fight() { 
        const {firstFighterState, secondFighterState} = this.state;

        this.registerHitsPowerListeners(firstFighterState, secondFighterState);
        this.registerBlocksPowerListeners(firstFighterState, secondFighterState);
        this.registerCriticalHitCombinationsListeners(firstFighterState, secondFighterState);

        return new Promise((resolve) => {
          document.addEventListener('winner', (e) => {
            const fighter =  e.detail.winner;
            resolve(fighter);
          });
        });
    }

    getDamage(attacker, defender) {
        const damage = this.getHitPower(attacker) - this.getBlockPower(defender);
        return Math.max(0, damage);
      }
      
    getHitPower(fighter) {
        const criticalHitChance = this.calcChance();
        const hitPower = fighter.power * criticalHitChance;
        return hitPower;
      }
      
    getBlockPower(fighter) {
        const dodgeChance = this.calcChance();
        const blockPower = fighter.defense * dodgeChance;
        return blockPower;
      }
      
    getCriticalDamage(attacker) {
        return 2 * attacker.power;
      }
      
    calcChance() {
        return Math.random() + 1;
      }

    attack(attackerState, defenderState) {
        const damage = this.getDamage(attackerState.fighterInfo, defenderState.fighterInfo);

        if (damage > 0) {
            defenderState.fighterInfo.health -= damage;
            this.writeLog(attackerState.fighterInfo, defenderState.fighterInfo, damage, defenderState.fighterInfo.health);
            this.updateHealthBar(defenderState, attackerState);
        }
    }

    criticalAttack(attackerState, defenderState) {
        const damage = this.getCriticalDamage(attackerState.fighterInfo);
        defenderState.fighterInfo.health -= damage; 
        this.writeLog(attackerState.fighterInfo, defenderState.fighterInfo, damage, defenderState.fighterInfo.health);
        this.updateHealthBar(defenderState, attackerState);
        this.disableCriticalHitCombination(attackerState);
    }

    writeLog(attacker, defender, damage, restHealth) {
        const logMessage = `The fighter ${attacker.name} attacked the fighter ${defender.name} and caused him damage -${damage}. The health of ${defender.name} is ${restHealth}.`;
        this.log.push(logMessage);
    }

    registerHitsPowerListeners(firstFighterState, secondFighterState) {
        const onKeyDown = (event) => {
            switch(event.code){
              case controls.PlayerOneAttack:
                if (!firstFighterState.isProtected && !secondFighterState.isProtected) {
                  this.attack(firstFighterState, secondFighterState);
                } 
                break;
        
              case controls.PlayerTwoAttack:
                if (!firstFighterState.isProtected && !secondFighterState.isProtected) {
                    this.attack(secondFighterState, firstFighterState);
                } 
                break;
            }
        };
        
        document.addEventListener('keydown', onKeyDown);

        document.addEventListener('deleteEvents', (e) => {
            document.removeEventListener('keydown', onKeyDown);
        });
      }
      
    registerBlocksPowerListeners(firstFighterState, secondFighterState) {
      
        const onKeyDown = (event) => {
          switch(event.code){
      
            case controls.PlayerOneBlock:
              if (!firstFighterState.isProtected) {
                firstFighterState.isProtected = true;
              }        
              break;
      
            case controls.PlayerTwoBlock:
              if (!secondFighterState.isProtected) {
                secondFighterState.isProtected = true;
              }  
              break;
          }
        };
      
        const onKeyUp = (event) => {
          switch(event.code){
      
            case controls.PlayerOneBlock:
              if(firstFighterState.isProtected) {
                firstFighterState.isProtected = false;
              }
              break;
      
            case controls.PlayerTwoBlock:
              if(secondFighterState.isProtected) {
                secondFighterState.isProtected = false;
              }
              break;
          }
        };
      
        document.addEventListener('keydown', onKeyDown);
        document.addEventListener('keyup', onKeyUp);

        document.addEventListener('deleteEvents', (e) => {
            document.removeEventListener('keydown', onKeyDown);
            document.removeEventListener('keyup', onKeyUp);
        });
      }
      
    registerCriticalHitCombinationsListeners(firstFighterState, secondFighterState) {
        const [KeyQ, KeyW, KeyE] = controls.PlayerOneCriticalHitCombination;
        const [KeyU, KeyI, KeyO] = controls.PlayerTwoCriticalHitCombination;
      
        const firstPlayerCriticalCombination = new Set();
        const secondPlayerCriticalCombination = new Set();
      
        const onKeyDown = (event) => {   
          switch (event.code) {
            case KeyQ:
              if (firstFighterState.CriticalHitCombinationAvailable)
              firstPlayerCriticalCombination.add(KeyQ);
              break;

            case KeyW:
              if (firstFighterState.CriticalHitCombinationAvailable)  
              firstPlayerCriticalCombination.add(KeyW);
              break;

            case KeyE:
              if (firstFighterState.CriticalHitCombinationAvailable)
              firstPlayerCriticalCombination.add(KeyE);
              break;

              case KeyU:
              if (secondFighterState.CriticalHitCombinationAvailable)
                secondPlayerCriticalCombination.add(KeyU);
              break;

            case KeyI:
              if (secondFighterState.CriticalHitCombinationAvailable)  
              secondPlayerCriticalCombination.add(KeyI);
              break;

            case KeyO:
              if (secondFighterState.CriticalHitCombinationAvailable)
              secondPlayerCriticalCombination.add(KeyO);
              break;
          }
          
          if (firstPlayerCriticalCombination.size === controls.PlayerOneCriticalHitCombination.length) {
            this.criticalAttack(firstFighterState, secondFighterState);
            firstPlayerCriticalCombination.clear();
          } 

          if (secondPlayerCriticalCombination.size === controls.PlayerTwoCriticalHitCombination.length) {
            this.criticalAttack(secondFighterState, firstFighterState);
            secondPlayerCriticalCombination.clear();
          } 
        };
      
        const onKeyUp = (event) => {
          switch (event.code) {
            case KeyQ:
                firstPlayerCriticalCombination.delete(KeyQ);
              break;

            case KeyW:
                firstPlayerCriticalCombination.delete(KeyW);
              break;

            case KeyE:
                firstPlayerCriticalCombination.delete(KeyE);
              break;

              case KeyU:
              secondPlayerCriticalCombination.delete(KeyU);
              break;

            case KeyI:
                secondPlayerCriticalCombination.delete(KeyI);
              break;

            case KeyO:
                secondPlayerCriticalCombination.delete(KeyO);
              break;
          } 
        };
      
        document.addEventListener('keydown', onKeyDown);
        document.addEventListener('keyup', onKeyUp);
      
        document.addEventListener('deleteEvents', (e) => {
            document.removeEventListener('keydown', onKeyDown);
            document.removeEventListener('keyup', onKeyUp);
          });
      }
 
    updateHealthBar(defenderState, attackerState) {      
        let fullHealthVal = defenderState.fullHealthVal;
        let restHealth = defenderState.fighterInfo.health;
        
        let result = (100 * restHealth) / fullHealthVal;

        defenderState.healthBarWidth = Math.max(0, result);
        
        this.setState({...defenderState, ...attackerState});
        if (result <= 0) {
          this.dispatchWinner(attackerState.fighterInfo);
        } 
      }
      
    dispatchWinner(winner) {
        const winnerCustomEvent = new CustomEvent('winner', { detail: {
          winner: winner
        }} );
        const deleteEventsCustomEvent = new CustomEvent('deleteEvents');
        document.dispatchEvent(winnerCustomEvent);
        document.dispatchEvent(deleteEventsCustomEvent);
      }
      
    disableCriticalHitCombination(fighterState) {
        fighterState.CriticalHitCombinationAvailable = false;
        setTimeout(() => {
          fighterState.CriticalHitCombinationAvailable = true;
        }, 10000);
      }
    
    render() {
        const {firstFighterState, secondFighterState} = this.state;

        return (
            <div className="arena___root">
                <div className="arena___fight-status">
                    <FighterIndicator 
                        fighter={firstFighterState.fighterInfo} 
                        healthBarWidth={firstFighterState.healthBarWidth}
                        />
                    <FighterIndicator 
                        fighter={secondFighterState.fighterInfo}
                        healthBarWidth={secondFighterState.healthBarWidth}
                        />
                </div>
            </div>
        )
    }
}

export default Arena;