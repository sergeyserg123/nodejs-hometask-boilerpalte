import React from "react";

export default function FighterIndicator({fighter, healthBarWidth}) {

    return (
        <div className="arena___fighter-indicator">
            <span className="arena___fighter-name">{fighter.name}</span>
            <div className="arena___health-indicator">
                <div className="arena___health-bar" style={{width: `${healthBarWidth}%`}}>
                
                </div>
            </div>
        </div>
    )
}