import React from 'react';
import { createFight } from '../../services/domainRequest/fightRequest';
import { Button } from '@material-ui/core';
import './showWinner.css';

class ShowWinner extends React.Component {

    constructor() {
        super();
        this.state = {
            loading: false,
            isSavedFight: false
        }
    }

    onShowFightsStory = () => {
        this.props.onShowFightsStory();
    }

    onSaveFight = async () => {
        const {fight} = this.props;
        this.setState({loading: true});
        const data = await createFight(fight);
        if (data && !data.error) {
            this.setState({loading: false, isSavedFight: true});
        }
    }

    render() {
        const { winner } = this.props;
        const { loading, isSavedFight } = this.state;

        if (isSavedFight && !loading) {
            return (
                <div className="root__container">
                    <h1>Fight saved!</h1>
                    <Button onClick={this.onShowFightsStory} variant="contained" color="primary">Show fights</Button>
                </div>
            )
        }

        if (loading) {
            return (
                <h5>Loading...</h5>
            )
        }

        return (
            <div className="root__container">
                <h1>Fighter {winner.name} is winner!</h1>
                <Button onClick={this.onSaveFight} variant="contained" color="primary">Save fight</Button>
            </div>
            
        )
    }
}

export default ShowWinner;