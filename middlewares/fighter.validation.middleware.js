const {validateRequest} = require('../validators/request.validator');
const Joi = require('joi');

const createFighterValid = (req, res, next) => {
    const schema = Joi.object({
        name: Joi.string().required(),
        power: Joi.number().greater(0).less(100).required(),
        defense: Joi.number().min(1).max(10).required(),
    });

    const isValid = validateRequest(req, res, schema);

    if(isValid) {
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    const schema = Joi.object({
        name: Joi.string().required().optional(),
        power: Joi.number().greater(0).max(100).required().optional(),
        defense: Joi.number().min(1).max(10).required().optional(),
        createdAt: Joi.optional(),
        updatedAt: Joi.optional()
    });

    const isValid = validateRequest(req, res, schema);

    if(isValid) {
        next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;