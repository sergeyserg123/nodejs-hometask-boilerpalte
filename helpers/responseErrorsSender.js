const statusCodes = require('../constants/statusCodes');

const responseErrorsSender = (error, response) => {
    const extractedErrors = [];

    error.details.map(err => extractedErrors.push({[err.path[0]] : err.message}));

    return response.status(statusCodes.BAD_REQUEST).json({
        error: true,
        messages: extractedErrors
    });
}

exports.responseErrorsSender = responseErrorsSender;