const {responseErrorsSender} = require('../helpers/responseErrorsSender');

const validateRequest = (req, res, schema) => {
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: false // ignore unknown props
    };
    const { error, value } = schema.validate(req.body, options);
    

    if (error) {
        responseErrorsSender(error, res)
    } else {
        req.body = value;
        return true;
    }
}

exports.validateRequest = validateRequest;