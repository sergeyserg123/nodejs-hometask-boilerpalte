import React from 'react';
import { Button } from '@material-ui/core';
import { getFights } from '../../services/domainRequest/fightRequest';
import './fightsStory.css';

class FightsStory extends React.Component {
    constructor() {
        super();
        this.state = {
            fights: [],
            selectedFight: null
        }
    }

    componentDidMount() {
        getFights().then(fights => {
            this.setState( {fights: [...fights] })
        })
    }

    showLog = (fightId) => {
        const fight = this.state.fights.find(fight => fight.id === fightId);
        if (fight !== undefined) {
            this.setState({selectedFight: fight});
        }
    }

    backToFightsList = () => {
        this.setState({selectedFight: null})
    }

    onNewFight = () => {
        this.props.onNewFight()
    }

    render() {
        const {fights, selectedFight} = this.state;
        console.log(selectedFight);
        if (selectedFight !== null) {
            return (
                <div className="root__container">
                    <h1>Fight</h1>
                    {selectedFight.log.map((it, index) => (
                        <div className="story__item" key={index}>{it}</div>
                    ))}
                    <Button onClick={this.backToFightsList} variant="contained" color="primary">Back to list</Button>
                </div>
            )
        }

        return (
            <div className="root__container">
                <h1>All fights</h1>
                {fights.map((it, index) => (
                    <div onClick={() => this.showLog(it.id)} className="story__item" key={index}>Fighter Id:{it.fighter1} VS Fighter Id: {it.fighter2}.</div>
                ))}
                <Button onClick={this.onNewFight} variant="contained" color="primary">New fight</Button>
            </div>
            
        )
    }
}

export default FightsStory;