const { BaseRepository } = require('./baseRepository');

class FightRepository extends BaseRepository {
    constructor() {
        super('fights');
    }

    createFight(fightData) {
        return this.create(fightData);
    }
}

exports.FightRepository = new FightRepository();